#!/usr/bin/env python3

## solution.py inputfile
## Solution to satellite telemetry exercise for the Enlighten Programming Challenge.
##
## Jack Tillman - 22 FEB 2021
## 
## Note: I took the example output to mean, if you see 3 hits in a 5 min window, display
## the first seen hit. If it's required that we display a different log message, the
## behavior can be easily modified by picking a different value for hitsfound.

import csv
import json
import argparse
from os import path
from datetime import datetime, timezone
from collections import defaultdict


# test if time delta between two datetimes is within a 5 minute window, in microseconds
def time_delta(time1, time2):
    timewindow_microseconds = 3e+8
    rawdelta = datetime.strptime(time1,'%Y%m%d %H:%M:%S.%f') - datetime.strptime(time2,'%Y%m%d %H:%M:%S.%f')
    return abs( rawdelta.microseconds ) < timewindow_microseconds


# handle the difference between input and output timestamps, and convert default tzdata to NATO-Zulu designator
def format_timestamp(time1):
    return datetime.strptime(time1,'%Y%m%d %H:%M:%S.%f')\
                    .replace(tzinfo=timezone.utc)\
                    .isoformat(sep='T', timespec='milliseconds')\
                    .replace('+00:00', 'Z')

def main(args):
    # input filename - note, this could be easily extended to handle a list of files
    inputfile = args.inputfile[0]
    if not path.exists(inputfile):
        parser.error(f"The file {inputfile} does not exist. Please specify a file and re-run the script.")


    # our list of warnings found
    output=[]

    # data containing out of spec log messages per satellite ID
    data = {}

    # If this were to be used for a variety of input file types, I'd probably modify this to read
    # in the following two lists from a config file, instead of just manually setting them here.

    # field names, since this isn't included at first line of csv - this can be easily extended
    fields = [
        'timestamp',
        'satelliteId',
        'red-high-limit',
        'yellow-high-limit',
        'yellow-low-limit',
        'red-low-limit',
        'raw-value',
        'component'
        ]

    # components, in example this is BATT and TSTAT 
    components = ['BATT','TSTAT'] #this can be easily extended to handle other data

    # nested dicts allow for straightforward abstraction/manipulation of data using multiple indices
    nested_dict = lambda: defaultdict(nested_dict)

    # hashmap of out of spec data 'hits' counts
    hitsfound = nested_dict()
    # Ex:
    # hitsfound["RED_HIGH"]["TSTAT"]=0 #note, this gets auto-extended if we add more components

    # hashmap of 'warning' dicts, we make a list of these and json.dumps to the screen at the end
    warning = nested_dict()
    # Ex:
    # warning["RED-LOW"]["BATT"] = {
    #      "satelliteId": "1234",
    #      "severity": "RED_LOW",
    #      "component": "BATT",
    #      "timestamp": "2018-01-01T23:05:07.421Z"
    # } #etc, can add more fields as needed



    with open(inputfile,'r') as csvfile:
        csvitem=csv.DictReader(csvfile, delimiter='|', fieldnames=fields)
        for row in csvitem:
            if float(row['raw-value']) < float(row['red-low-limit']):
                severity = 'RED_LOW'
            elif float(row['raw-value']) > float(row['red-high-limit']):
                severity = 'RED_HIGH'
            else:
                continue # not out of spec, skip to next line of csv

            # handling new keys
            if row['satelliteId'] not in data:
                data[row['satelliteId']] = [row]
            else:
                data[row['satelliteId']].append(row)

            # reset the hits counter for each line read from the csvfile
            for component in components:
                hitsfound[severity][component]=0

            # compare current out-of-spec data point to all previous out-of-spec data points, checking for time relevance
            for warnings in data[row['satelliteId']]:
                for component in components:
                    if row['component'] == warnings['component'] == component and time_delta(row['timestamp'],warnings['timestamp']): #time check
                        hitsfound[severity][component]+=1
                        # I took the example output to mean, if you see 3 hits in a 5 min window, display the first seen log message
                        if hitsfound[severity][component] == 1:
                            warning[severity][component]['satelliteId'] = row['satelliteId']
                            warning[severity][component]['severity'] = severity
                            warning[severity][component]['component'] = row['component']
                            warning[severity][component]['timestamp'] = format_timestamp(row['timestamp'])
                    if hitsfound[severity][component] == 3:
                        output.append(warning[severity][component])
                        hitsfound[severity][component]+=1

    print(json.dumps(output, indent=4)) ##end def main(args):


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Ingest status telemetry data and create alert messages')
    parser.add_argument('inputfile',
                       type=str,
                       nargs=1,
                       help='the path to telemetry data file')

    args = parser.parse_args()
    main(args) ##end if __name__ == '__main__':
